# Computer Break

This short script---and the corresponding systemd service/timer---sends desktop notifications to a Linux desktop hourly. The text of the notification is randomly chosen from a list of strings which are exercises. These exercises can be customized.

## Message List

The script uses `messages.list` if found or fallse back to `messages-example.list` distributed with the source. This way it is possible to customize the messages used in the notification.

The format of the file is the following:
- every line is considered as a single possible random message,
- empty lines, and lines starting with `#` are ignored,
- icons can be specified on a per-line basis. The format should be `message=icon-path`. Relative paths are converted to absolute, using python3 `os.path.abspath` command.

## Running it with systemd

An example systemd service and timer file is present. Copy these files to the `/home/myusername/.config/systemd/user/` path, input the real path in computer-break.service (change the string `/FULL/PATH/TO/SOURCE/` in both 2 lines) and pass the following commands:

```
# reread the new service and timer file
systemctl --user daemon-reload
# the following is the test the service:
systemctl start computer-break.service
# if the service ran succesfully than make it happen every hour
systemctl start computer-break.timer
systemctl enable computer-break.timer
```

## Attribution
Icon packs used:
- https://www.flaticon.com/license/pack/2647478
- https://www.flaticon.com/license/pack/2647639
- https://www.flaticon.com/license/pack/2789797
- https://www.flaticon.com/license/pack/3382652

SpiderMan icon from Mike Ashley (https://icon-icons.com/icon/spiderman/44260)
