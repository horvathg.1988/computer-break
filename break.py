#! /usr/bin/env python3

import notify2
import os
import random
import subprocess
import datetime

msg_file_name = "messages-example.list"
if os.path.isfile("messages.list"):
    msg_file_name = "messages.list"
print("File used as source: ", msg_file_name)

with open(msg_file_name) as my_file:
    messages_array = my_file.readlines()

# remove empty elements
try:
    while(True):
        messages_array.remove("\n")
except ValueError:
    pass

# handle comments
messages_array = [item for item in messages_array if item[0] !='#']

message_line = random.choice(messages_array)
if "=" in message_line:
    (message_string, message_icon) = message_line.split("=",2)
else:
    message_string = message_line
    message_icon = ""

print("Line to show:", message_line.rstrip())
print("Message string:", message_string.rstrip())
print("Optional icon path (if any):", message_icon.rstrip())

notify2.init("computer-break")
n = notify2.Notification("Computer usage break", message_string, os.path.abspath(message_icon).rstrip())
n.set_timeout(10000)
n.set_urgency(notify2.URGENCY_CRITICAL)
n.show()

log_file_name="break-messages.log"
current_date = datetime.datetime.today()
current_date_string = datetime.datetime.strftime(current_date,"%Y-%m-%d %H:%M")
yesterday_string = datetime.datetime.strftime( (current_date - datetime.timedelta(days=1)) ,"%Y-%m-%d")
print("yesterday string: ", yesterday_string)

from subprocess import Popen, PIPE
f = log_file_name
# Get the last line from the file
p = Popen(['tail','-1',f],shell=False, stderr=PIPE, stdout=PIPE)
res,err = p.communicate()
if err:
    print (err.decode())
    last_log_date_string = "2000-01-01"
else:
    # Use split to get the part of the line that you require
    last_log_date_string = res.decode().split(' ')[1]
    print("last date in log: ", last_log_date_string)

# Save messages to a file
# Starts with TODO which can be changed to DONE if some UI is created
# Space must be stripeed in case of empty image
with open(log_file_name, "a") as myfile:
    if(last_log_date_string == yesterday_string):
        myfile.write("# -------------------------\n")
    myfile.write(" ".join(("TODO", current_date_string, message_string, message_icon)).strip(" "))
